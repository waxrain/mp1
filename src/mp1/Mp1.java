/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mp1;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author Feicia
 */
public class Mp1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int n = 0; // default exp value
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        BigInteger result = BigInteger.ZERO; // n = 30 does not fit into long
        for (int i = 1; i <= n; i++){ // i = number of 3s
            BigInteger temp2 = new BigDecimal(Math.pow(3, n-i)).toBigInteger();
            BigInteger combi = combination(n, i);
            result = result.add(temp2.multiply(combi).multiply(new BigInteger(Integer.toUnsignedString(i))));
        }
        System.out.println(result);
    }
    
    public static BigInteger combination(int N, int K) {
        BigInteger result = BigInteger.ONE;
        for (int k = 0; k < K; k++) {
            result = result.multiply(BigInteger.valueOf(N - k)).divide(BigInteger.valueOf(k + 1));
        }
        return result;
    }
}
